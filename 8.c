//http://www.dailyfreecode.com/code/multiply-two-matrices-thread-1104.aspx

# include <stdio.h>
# include <pthread.h>
#include "vars.h"
#include "p_mult.h"

int main()
{
    pthread_t tid;    
    int iCount,jCount,kCount;
    
    for(iCount=0;iCount<r1;iCount++)
    {
        for(jCount=0;jCount<c1;jCount++)
        {
		MAT1[iCount][jCount] = rand()%10 +1;
        }
    }
    
    for(iCount=0;iCount<r2;iCount++)
    {
        for(jCount=0;jCount<c2;jCount++)
        {
		MAT2[iCount][jCount] = rand()%10 +1;
        }
    }
    
    if(c1!=r2)
    {
        printf("Multipication of Matrix not Possible !!!");
    }
    else
    {
        for(iCount=0;iCount<r1;iCount=iCount+2)
        {
            for(jCount=0;jCount<c2;jCount=jCount+2)
            {
                MAT3[iCount][jCount]=0;
            }
        }
        
        pthread_create(&tid,NULL,thread_Multiply_Matrix,NULL);
        
        for(iCount=0;iCount<r1;iCount=iCount+2)
        {
            for(jCount=0;jCount<c2;jCount++)
            {
                for(kCount=0;kCount<c1;kCount++)
                {
                    MAT3[iCount][jCount]+=MAT1[iCount][kCount] * MAT2[kCount][jCount];
                }
            }
        }
        
        pthread_join(tid,NULL);
    }
    
    printf("\n Matrix 1 \n");
    
    for(iCount=0;iCount<r1;iCount++)
    {
        for(jCount=0;jCount<c1;jCount++)
        {
            printf("%d \t",MAT1[iCount][jCount]);
        }
        printf("\n");
    }    
    
    printf("\n Matrix 2 \n");
    
    for(iCount=0;iCount<r2;iCount++)
    {
        for(jCount=0;jCount<c2;jCount++)
        {
            printf("%d \t",MAT2[iCount][jCount]);
        }
        printf("\n");
    }    
    
    printf("\n Multipication of Matrix ...\n");
    
    for(iCount=0;iCount<r1;iCount++)
    {
        for(jCount=0;jCount<c2;jCount++)
        {
            printf("%d \t",MAT3[iCount][jCount]);
        }
        printf("\n");
    }    
    return 0;
}
